#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <getopt.h>
#include <unistd.h>
#include <termios.h>
int LINELEN;
int PAGELEN;
int total_lines=0;
int total_read=0;
int per=0;
void do_more(FILE *,int num_of_lines);
int get_input(FILE *);
int main(int argc, char *argv[]){
	struct winsize window;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &window);
	PAGELEN= window.ws_row-1;
	LINELEN= window.ws_col-1;

	if(argc==1){
		printf("This is the help page\n");
		exit(0);
	}
	int i=0;
	FILE *f;
	int num=0;
	while(++i < argc){
		if(argc>2){
			printf("::::::::::::::\n");
			printf("%s",argv[i]);
			printf("\n::::::::::::::\n");
			
		}
		f = fopen(argv[i],"r");
		if(f==NULL){
			perror("Can not open the file");
			exit(1);
		}
		char ch;
		for(ch=getc(f);ch != EOF;ch=getc(f)){
			if(ch=='\n')
				total_lines++;
		}
		fseek(f,0,SEEK_SET);
		do_more(f,num);
		fclose(f);
		num=0;
		if(argc>2){
			if((i+1)!=argc){
				FILE * fp = fopen("/dev/tty","r");
				printf("\033[7m --more--(Next file: %s) \033[m",argv[i+1]);
				int c;
				c=getc(fp);
				printf("\033[1A \033[2K \033[1G");
				if(c==' ')
					num=3;
				else if(c=='\n')
					num=PAGELEN-1;
				else 
					break;
			}
		}
	}
	return 0;
}

void do_more(FILE *fp,int num_of_lines){
	char buffer[LINELEN];
	int rv;
	FILE * fp_tty = fopen("/dev/tty","r");
	while (fgets(buffer, LINELEN, fp)){
		fputs(buffer, stdout);
		num_of_lines++;
		total_read++;
		if(num_of_lines == PAGELEN){
			per=(total_read*100)/(total_lines);
			if(per==100)
				break;
			rv=get_input(fp_tty);
			if(rv==0){
				printf("\033[1A \033[2K \033[1G");
				break;
			}
			else if(rv==1){
				printf("\033[1A \033[2K \033[1G");
				num_of_lines-= PAGELEN;
			}
			else if(rv == 2){
				printf("\033[1A \033[2K \033[1G");
				num_of_lines-=1;
			}
			else if (rv == 3){
				printf("\033[1A \033[2K \033[1G");
				break;
			}
		}
	}
}

int get_input(FILE * cmdstream){
	int c;
	printf("\033[7m --more--(%d%%) \033[m",per);
	c=getc(cmdstream);
	if(c=='q')
		return 0;
	if(c==' ')
		return 1;
	if(c=='\n')
		return 2;
	return 3;
}
