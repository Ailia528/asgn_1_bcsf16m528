#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <ctype.h>
#include <errno.h>

extern int errno;
void do_find_name(char*,char *);
void do_find_type(char*,unsigned char );
int main(int argc, char* argv[]){ 
	if(strcmp(argv[2],"-name")==0)
		do_find_name(argv[1],argv[3]);
	else if(strcmp(argv[2],"-type")==0){
		unsigned char t;
		if(strcmp(argv[3],"b")==0)
			t=DT_BLK;
		else if(strcmp(argv[3],"c")==0)
			t=DT_CHR;
		else if(strcmp(argv[3],"d")==0)
			t=DT_DIR;
		else if(strcmp(argv[3],"p")==0)
			t=DT_FIFO;
		else if(strcmp(argv[3],"f")==0)
			t=DT_REG;
		else if(strcmp(argv[3],"l")==0)
			t=DT_LNK;
		else if(strcmp(argv[3],"s")==0)
			t=DT_SOCK;
		else 
			t=DT_UNKNOWN;
		do_find_type(argv[1],t);
	}
	return 0;
}
void do_find_name(char *dir,char * filename){
	struct dirent * entry;
	DIR * dp = opendir(dir);
	if (dp == NULL){
		fprintf(stderr, "Cannot open directory:%s\n",dir);
		return;
	}
	errno = 0;
	while((entry = readdir(dp)) != NULL){
		if(entry == NULL && errno != 0){
		   perror("readdir failed");
		   exit(errno);
		}
		else
			if(strcmp(entry->d_name,".")!=0 && strcmp(entry->d_name,"..")!=0){
		   		if(strcmp(entry->d_name,filename)==0){
		   			printf("%s/%s\n",dir,filename);
		   		}
		   		else{
		   			if(entry->d_type==DT_DIR){
		   				char buffer[1000]={'\0'};
		   				strcat(buffer,dir);
			   			strcat(buffer,"/");
			   			strcat(buffer,entry->d_name);
			   			do_find_name(buffer,filename);
		   			}
		   		}
		   	}
		}
	closedir(dp);
}
void do_find_type(char *dir,unsigned char type){
	struct dirent * entry;
	DIR * dp = opendir(dir);
	if (dp == NULL){
		fprintf(stderr, "Cannot open directory:%s\n",dir);
		return;
	}
	errno = 0;
	while((entry = readdir(dp)) != NULL){
		if(entry == NULL && errno != 0){
		   perror("readdir failed");
		   exit(errno);
		}
		else
			if(strcmp(entry->d_name,".")!=0 && strcmp(entry->d_name,"..")!=0){
		   		if(entry->d_type==type)
		   			printf("%s/%s\n",dir,entry->d_name);
		   		if(entry->d_type==DT_DIR){
	   				char buffer[1000]={'\0'};
	   				strcat(buffer,dir);
		   			strcat(buffer,"/");
		   			strcat(buffer,entry->d_name);
		   			do_find_type(buffer,type);
		   		}
		   	}
		}
	closedir(dp);
}
