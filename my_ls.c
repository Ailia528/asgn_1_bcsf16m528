#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include<sys/stat.h>
#include <stdlib.h>
#include <ctype.h> 
#include <errno.h>
int l_exist=0,i_exist=0;
extern int errno;
struct my_dirent {
    ino_t          d_ino;       /* inode number */
    char           d_name[256]; /* filename */
};
void do_ls(char*);
void Option_l(char *name);
void my_sort(struct my_dirent t[100],int c);
int main(int argc, char* argv[]){   
	int i=0,k=0;
	if (argc == 1)
		do_ls(".");
	else{
		for(i=1;i<argc;i++){
			if(strcmp(argv[i],"-l")==0)
				l_exist=1;
			else if(strcmp(argv[i],"-i")==0)
				i_exist=1;
			else if(strcmp(argv[i],"-li")==0 || strcmp(argv[i],"-il")==0){
				l_exist=1;
				i_exist=1;
			}
			else{
				i=i-1;
				k=1;
				break;
			}
		}
		int e=0;
		if(k==1)
			while(++i < argc){
				if(i!=argc-1 || e==1){
					e=1;
					if(i>1)
						printf("\n");	
			   		printf("%s:\n", argv[i] );
			   	}
				do_ls(argv[i]);
			}
		else
			do_ls(".");
	}
	return 0;
}
void do_ls(char * dir){
	int dr=0,c=0,i=0;
	struct dirent * entry;
	struct my_dirent t[100];
	DIR * dp = opendir(dir);
	if (dp == NULL){
		fprintf(stderr, "Cannot open directory:%s\n",dir);
		return;
	}
	errno = 0;
	while((entry = readdir(dp)) != NULL)
	   if(entry == NULL && errno != 0){
		    perror("readdir failed");
		    exit(1);
	   }else
	   	if(entry->d_name[0] != '.'){
	   	    dr=1;
	  	    strncpy(t[c].d_name, entry->d_name,sizeof(t[c].d_name));
	  	    t[c].d_ino=entry->d_ino;
	  	    c=c+1; 
		}
	my_sort(t,c);
	for (i = 0; i < c; i++) {
		char *ptr = strchr(t[i].d_name, ' ');
		if(i_exist==1)
			printf("%ld  ",t[i].d_ino);
		if(l_exist==1){
			char prefix[1000] = "";
			strcat(prefix, dir);
			strcat(prefix, "/");
			strcat(prefix, t[i].d_name);
			Option_l(prefix);
		}
		if(ptr!=NULL)
			printf("'%s'   ",t[i].d_name);
		else
	    	printf("%s   ",t[i].d_name);
	    if(l_exist==1){
			printf("\n");
		}
	}
	if(dr==1)
		printf("\n");
	closedir(dp);
}
void Option_l(char *fname){
	struct stat buf;
	if (lstat(fname, &buf)<0){ 
		perror("Error in stat");
		exit(1);
	}
	int mode = buf.st_mode; 
	char str[11];
	strcpy(str, "----------");
	if ((mode &  0170000) == 0010000) 
		str[0] = 'p';
	else if ((mode &  0170000) == 0020000) 
		str[0] = 'c';
	else if ((mode &  0170000) == 0040000) 
		str[0] = 'd';
	else if ((mode &  0170000) == 0060000) 
		str[0] = 'b';
	else if ((mode &  0170000) == 0100000) 
		str[0] = '-';
	else if ((mode &  0170000) == 0120000) 
		str[0] = 'l';
	else if ((mode &  0170000) == 0140000) 
		str[0] = 's';
	mode = buf.st_mode;
	if((mode & 0000400) == 0000400) str[1] = 'r';		//owner  permissions
	if((mode & 0000200) == 0000200) str[2] = 'w';
	if((mode & 0000100) == 0000100) str[3] = 'x';
	if((mode & 0000040) == 0000040) str[4] = 'r';		//group permissions
	if((mode & 0000020) == 0000020) str[5] = 'w';
	if((mode & 0000010) == 0000010) str[6] = 'x';
	if((mode & 0000004) == 0000004) str[7] = 'r';		//others  permissions
	if((mode & 0000002) == 0000002) str[8] = 'w';
	if((mode & 0000001) == 0000001) str[9] = 'x';
	if((mode & 0004000) == 0004000) str[3] = 's';		//special  permissions
	if((mode & 0002000) == 0002000) str[6] = 's';
	if((mode & 0001000) == 0001000) str[9] = 't';
	printf("%s ", str);

   printf("%ld ", buf.st_nlink);
   
   struct passwd * pwd = getpwuid(buf.st_uid);
   errno = 0;
   if (pwd == NULL){
      if (errno == 0)
         printf("Record not found in passwd file.\n");
      else
         perror("getpwuid failed");
   }
   else
       printf("%s ",pwd->pw_name);  
       
   struct group * grp = getgrgid(buf.st_gid);
   errno = 0;
   if (grp == NULL){
      if (errno == 0)
          printf("Record not found in /etc/group file.\n");
      else
          perror("getgrgid failed");
   }else
      printf("%s ", grp->gr_name); 
      
   printf("%ld ", buf.st_size);
   
   char * str_time=ctime(&buf.st_mtime);
   int l;
   for(l=4;l<=10;l++){
   	printf("%c", str_time[l]);
   }
   if(str_time[20]=='2' && str_time[21]=='0' && str_time[22]=='1' && str_time[23]=='9'){
   	printf("%c", str_time[11]);
   	printf("%c", str_time[12]);
   	printf("%c", str_time[13]);
   	printf("%c", str_time[14]);
   	printf("%c", str_time[15]);
   }
   else {
   	printf("%c", str_time[20]);
   	printf("%c", str_time[21]);
   	printf("%c", str_time[22]);
   	printf("%c", str_time[23]);
   }
   printf("  ");
}
void my_sort(struct my_dirent t[100],int c){
	int i,j;
	for (i = 0; i < c - 1 ; i++)
		for (j = i + 1; j < c; j++){
			if (strcmp(t[i].d_name, t[j].d_name) > 0) {
				char temp[250];
				strncpy(temp, t[i].d_name,sizeof(temp));
				strncpy(t[i].d_name, t[j].d_name,sizeof(t[i].d_name));
				strncpy(t[j].d_name, temp,sizeof(t[j].d_name));
				ino_t g;
				g=t[i].d_ino;
				t[i].d_ino=t[j].d_ino;
				t[j].d_ino=g;
			}
		}
}
